.. Workday-notes documentation master file, created by
   sphinx-quickstart on Fri Jun 21 11:35:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Notes on Workday
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Integrations <integrations>
   Workday Studio <workday_studio>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
